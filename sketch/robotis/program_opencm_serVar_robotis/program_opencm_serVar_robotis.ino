//  https://toglefritz.com/hexapod-inverse-kinematics-equations/

byte coxa[]= {
  1, 4, 7, 16, 13, 10};
byte femur[]={
  2, 5, 8, 17, 14, 11};
byte tibia[]={
  3, 6, 9, 18, 15, 12};

#define DXL_BUS_SERIAL1 1  //Dynamixel on Serial1(USART1)  <-OpenCM9.04
#define DXL_BUS_SERIAL2 2  //Dynamixel on Serial2(USART2)  <-LN101,BT210
#define DXL_BUS_SERIAL3 3  //Dynamixel on Serial3(USART3)  <-OpenCM 485EXP

boolean moving = false;
int normalspeed = 0;
int specialSpeed = 0;
Dynamixel Dxl(DXL_BUS_SERIAL3);

int CoxaServo[6];
int FemurServo[6];
int TibiaServo[6];

int sCoxaServo[6];
int sFemurServo[6];
int sTibiaServo[6];

int trcoxa[]= {
  -50, -50, -50, 50, 50, 50};
int trfemur[]={
  0, 0, 0, 0, 0, 0};
int trtibia[]={
  0, 0, 0, 0, 0, 0};

int timer_step=5;
int step=0;
int count=0;

byte dataPin=3;
byte clockPin=2;
byte controlPin=1;

int mode=0,x=0,y=0,z=0;

byte data=0,ibyte=0,indexs=0;

byte buff[7];

void int0(void){

  if(digitalRead(controlPin)==1){
    digitalWrite(BOARD_LED_PIN,0);
    data = data | (digitalRead(dataPin) << ibyte); 
    ibyte++;
    if(ibyte>=8){
      buff[indexs]=data;
      indexs++;
      ibyte=0;
      data=0;
    }
  }

}

void int1(void){
  mode=buff[0];
  x = buff[1] << 8|buff[2];
  y = buff[3] << 8|buff[4];
  z = buff[5] << 8|buff[6];
  ibyte=0;
  data=0;
  indexs=0;
  digitalWrite(BOARD_LED_PIN,1);
}

void run_servo(){
  for(int i=0; i<6; i++){
    Dxl.setPosition((coxa[i]),CoxaServo[i],sCoxaServo[i]);
    Dxl.setPosition((femur[i]),FemurServo[i],sFemurServo[i]);
    Dxl.setPosition((tibia[i]),TibiaServo[i],sTibiaServo[i]);
  }
}

void standby(){
  for(int i=0; i<6; i++){
    CoxaServo[i]=512-trcoxa[i];  
    sCoxaServo[i]=500;
    FemurServo[i]=512-trfemur[i]; 
    sFemurServo[i]=500;
    TibiaServo[i]=512-trtibia[i]; 
    sTibiaServo[i]=500;
  }
}

void set_speed(int speed){
  normalspeed=speed;
  for(int i=0; i<6; i++){
    sCoxaServo[i]=speed;
    sFemurServo[i]=speed;
    sTibiaServo[i]=speed;
  }
}
void set_speed_leg(int speed){
  specialSpeed=speed;
  for(int i=0; i<6; i++){
    sFemurServo[i]=speed;
  }
}

#define MOVING 46
boolean isMoving(){
  boolean isMove = false;
  for(int i=0; i<6; i++){
    isMove = Dxl.readByte(coxa[i], MOVING); if(isMove) break;
    isMove = Dxl.readByte(femur[i], MOVING);if(isMove) break;
    isMove = Dxl.readByte(tibia[i], MOVING);if(isMove) break;
  }
  return isMove;
}

void run_leg(int x, int y, int z){
  int pos=512, up=60, down=-20, tbin=0, tbout=-80;
  set_speed(z);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4];

    CoxaServo[0]=pos-y-trcoxa[0];
    CoxaServo[2]=pos-y-trcoxa[2];
    CoxaServo[4]=pos+x-trcoxa[4];

    TibiaServo[0]=pos-trtibia[0];
    TibiaServo[2]=pos-tbin-trtibia[2];

    CoxaServo[1]=pos+y-trcoxa[1];
    CoxaServo[3]=pos-x-trcoxa[3];
    CoxaServo[5]=pos-x-trcoxa[5];

    TibiaServo[3]=pos-tbout-trtibia[3];
    TibiaServo[5]=pos-trtibia[5];
  }
  if(step==1){
    FemurServo[0]=pos-down-trfemur[0];
    FemurServo[2]=pos-trfemur[2];
    FemurServo[4]=pos-trfemur[4];

    FemurServo[3]=pos-(-10)-trfemur[3];
  }
  if(step==2){
    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    CoxaServo[1]=pos-y-trcoxa[1];  
    CoxaServo[3]=pos+x-trcoxa[3];  
    CoxaServo[5]=pos+x-trcoxa[5];  

    TibiaServo[3]=pos-tbin-trtibia[0]; 
    TibiaServo[5]=pos-(-50)-trtibia[2]; 

    CoxaServo[0]=pos+y-trcoxa[0];  
    CoxaServo[2]=pos+y-trcoxa[2];  
    CoxaServo[4]=pos-x-trcoxa[4];  

    TibiaServo[0]=pos-trtibia[0]; 
    TibiaServo[2]=pos-tbout-trtibia[2]; 
  }
  if(step==3){ 
    FemurServo[1]=pos-trfemur[1]; 
    FemurServo[3]=pos-trfemur[3]; 
    FemurServo[5]=pos-down-trfemur[5]; 

    FemurServo[2]=pos-(-10)-trfemur[2]; 

  }
}

void leg_putar(int x, int y, int z){
  int pos=512, up=120, down=-35, tbin=20, tbout=-120;
  set_speed(z);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    CoxaServo[0]=pos-y-trcoxa[0];  
    CoxaServo[2]=pos-y-trcoxa[2];  
    CoxaServo[4]=pos+x-trcoxa[4];  

    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4]; 

    CoxaServo[1]=pos+y-trcoxa[1]; 
    CoxaServo[3]=pos-x-trcoxa[3]; 
    CoxaServo[5]=pos-x-trcoxa[5]; 
  }
  if(step==1){
    FemurServo[0]=pos-trfemur[0];
    FemurServo[2]=pos-trfemur[2]; 
    FemurServo[4]=pos-trfemur[4];
  }
  if(step==2){
    CoxaServo[1]=pos-y-trcoxa[1];
    CoxaServo[3]=pos+x-trcoxa[3]; 
    CoxaServo[5]=pos+x-trcoxa[5];

    FemurServo[1]=pos-up-trfemur[1];
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    CoxaServo[0]=pos+y-trcoxa[0]; 
    CoxaServo[2]=pos+y-trcoxa[2]; 
    CoxaServo[4]=pos-x-trcoxa[4]; 
  }
  if(step==3){ 
    FemurServo[1]=pos-trfemur[1]; 
    FemurServo[3]=pos-trfemur[3];
    FemurServo[5]=pos-trfemur[5]; 
  }
}

void geser_kiri(int z){
  int pos=512, up=35, down=-50, tbin=30, tbout=-30;
  set_speed(z);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    TibiaServo[0]=pos-tbin; 
    TibiaServo[2]=pos-tbin; 
    TibiaServo[4]=pos-tbout; 

    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4]; 

    TibiaServo[1]=pos-tbout; 
    TibiaServo[3]=pos-tbin; 
    TibiaServo[5]=pos-tbin; 
  }
  if(step==1){
    FemurServo[0]=pos-down-trfemur[0]; 
    FemurServo[2]=pos-down-trfemur[2]; 
    FemurServo[4]=pos-down-trfemur[4]; 
  }
  if(step==2){
    TibiaServo[0]=pos-tbout;
    TibiaServo[2]=pos-tbout;
    TibiaServo[4]=pos-tbin; 

    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    TibiaServo[1]=pos-tbin; 
    TibiaServo[3]=pos-tbout; 
    TibiaServo[5]=pos-tbout; 
  }
  if(step==3){ 
    FemurServo[1]=pos-down-trfemur[1]; 
    FemurServo[3]=pos-down-trfemur[3]; 
    FemurServo[5]=pos-down-trfemur[5]; 
  }
}

void geser_kanan(int z){
  int pos=512, up=35, down=-50, tbin=30, tbout=-30;
  set_speed(z);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    TibiaServo[0]=pos-tbout; 
    TibiaServo[2]=pos-tbout; 
    TibiaServo[4]=pos-tbin; 

    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4]; 

    TibiaServo[1]=pos-tbin;
    TibiaServo[3]=pos-tbout;
    TibiaServo[5]=pos-tbout; 
  }
  if(step==1){
    FemurServo[0]=pos-down-trfemur[0]; 
    FemurServo[2]=pos-down-trfemur[2]; 
    FemurServo[4]=pos-down-trfemur[4]; 
  }
  if(step==2){
    TibiaServo[0]=pos-tbin; 
    TibiaServo[2]=pos-tbin;
    TibiaServo[4]=pos-tbout; 

    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    TibiaServo[1]=pos-tbout; 
    TibiaServo[3]=pos-tbin; 
    TibiaServo[5]=pos-tbin; 
  }
  if(step==3){ 
    FemurServo[1]=pos-down-trfemur[1]; 
    FemurServo[3]=pos-down-trfemur[3]; 
    FemurServo[5]=pos-down-trfemur[5]; 
  }
}

void semprot_api(int x, int y, int z){
  int pos=512;
  count++;
  if(count>=10){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    CoxaServo[0]=pos-y-trcoxa[0];  
    sCoxaServo[0]=z;
    CoxaServo[1]=pos-y-trcoxa[1];  
    sCoxaServo[1]=z;  
    CoxaServo[2]=pos-y-trcoxa[2];  
    sCoxaServo[2]=z;

    CoxaServo[3]=pos-x-trcoxa[3];  
    sCoxaServo[3]=z;
    CoxaServo[4]=pos-x-trcoxa[4];  
    sCoxaServo[4]=z;  
    CoxaServo[5]=pos-x-trcoxa[5];  
    sCoxaServo[5]=z;
  }
  if(step==1){
    CoxaServo[0]=pos-trcoxa[0];  
    sCoxaServo[0]=z;
    CoxaServo[1]=pos-trcoxa[1];  
    sCoxaServo[1]=z;  
    CoxaServo[2]=pos-trcoxa[2];  
    sCoxaServo[2]=z;

    CoxaServo[3]=pos-trcoxa[3];  
    sCoxaServo[3]=z;
    CoxaServo[4]=pos-trcoxa[4];  
    sCoxaServo[4]=z;  
    CoxaServo[5]=pos-trcoxa[5];  
    sCoxaServo[5]=z;
  }
  if(step==2){
    CoxaServo[0]=pos+y-trcoxa[0];  
    sCoxaServo[0]=z;
    CoxaServo[1]=pos+y-trcoxa[1];  
    sCoxaServo[1]=z;  
    CoxaServo[2]=pos+y-trcoxa[2];  
    sCoxaServo[2]=z;

    CoxaServo[3]=pos+x-trcoxa[3];  
    sCoxaServo[3]=z;
    CoxaServo[4]=pos+x-trcoxa[4];  
    sCoxaServo[4]=z;  
    CoxaServo[5]=pos+x-trcoxa[5];  
    sCoxaServo[5]=z;
  }
  if(step==3){
    CoxaServo[0]=pos-trcoxa[0];  
    sCoxaServo[0]=z;
    CoxaServo[1]=pos-trcoxa[1];  
    sCoxaServo[1]=z;  
    CoxaServo[2]=pos-trcoxa[2];  
    sCoxaServo[2]=z;

    CoxaServo[3]=pos-trcoxa[3];  
    sCoxaServo[3]=z;
    CoxaServo[4]=pos-trcoxa[4];  
    sCoxaServo[4]=z;  
    CoxaServo[5]=pos-trcoxa[5];  
    sCoxaServo[5]=z;
  }
}
void setup() 
{ 
  SerialUSB.begin();

  pinMode(BOARD_LED_PIN, OUTPUT);
  pinMode(dataPin, INPUT_PULLDOWN);
  pinMode(clockPin, INPUT_PULLDOWN);
  pinMode(controlPin, INPUT_PULLDOWN);
  attachInterrupt(clockPin,int0, RISING);
  attachInterrupt(controlPin,int1, FALLING);
  digitalWrite(BOARD_LED_PIN, 1); 

  Dxl.begin(3);  
  for(int i = 0; i<6; i++)
  {
    Dxl.jointMode(coxa[i]); 
    Dxl.jointMode(femur[i]); 
    Dxl.jointMode(tibia[i]); 
  }

  standby();
  run_servo();
  /*
  delay(1000);
    serVar(120,1024,30);//speed, timer, high
    x=60;y=60;z=90;  //test maju kompas
    mode=6; step=0;
  /*
  //x=80;y=80;z=100;//test maju
  //mode=1;
  */
//  x=50;y=50;z=200;  //test wall kanan
//  mode=8;
} 

void loop() 
{ 

  if(mode==0)standby();
  if(mode==1)run_leg(x,y,z); 
  if(mode==2)leg_putar(x,y,z); 
  if(mode==3)geser_kiri(z);
  if(mode==4)geser_kanan(z);
  if(mode==5)semprot_api(x, y, z);
  if(mode==6)run_leg_height(x,y,z);
  if(mode==7)serVar(x,y,z); //(7,moveSpeed,upSpeed,stepDelay);
  if(mode==8)run_leg_wall(x,y,z); 
  run_servo();
  

//  if(digitalRead(BOARD_LED_PIN)==1){
//    SerialUSB.print(mode);
//    SerialUSB.print(" ");
//    SerialUSB.print(x);
//    SerialUSB.print(" ");
//    SerialUSB.print(y);
//    SerialUSB.print(" ");
//    SerialUSB.print(z);
//    SerialUSB.println();
//  }

} 
void run_leg_height(int x, int y, int z){
  int pos=512, up=z, down=-20, tbin=0, tbout=-80;
  //set_speed(z);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4];
    //TibiaServo[4]=pos+tbout-trtibia[4];

    CoxaServo[0]=pos-y-trcoxa[0];
    CoxaServo[2]=pos-y-trcoxa[2];
    CoxaServo[4]=pos+x-trcoxa[4];

    TibiaServo[0]=pos-trtibia[0];
    TibiaServo[2]=pos-tbin-trtibia[2];

    CoxaServo[1]=pos+y-trcoxa[1];
    CoxaServo[3]=pos-x-trcoxa[3];
    CoxaServo[5]=pos-x-trcoxa[5];

    TibiaServo[3]=pos-tbout-trtibia[3];
    TibiaServo[5]=pos-trtibia[5];
  }
  if(step==1){
    FemurServo[0]=pos-down-trfemur[0];
    FemurServo[2]=pos-trfemur[2];
    FemurServo[4]=pos-trfemur[4];
    //TibiaServo[4]=pos-trtibia[4];

    FemurServo[3]=pos-(-10)-trfemur[3];
  }
  if(step==2){
    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 
    //TibiaServo[1]=pos+tbout-trtibia[1];

    CoxaServo[1]=pos-y-trcoxa[1];  
    CoxaServo[3]=pos+x-trcoxa[3];  
    CoxaServo[5]=pos+x-trcoxa[5];  

    TibiaServo[3]=pos-tbin-trtibia[0]; 
    TibiaServo[5]=pos-(-50)-trtibia[2]; 

    CoxaServo[0]=pos+y-trcoxa[0];  
    CoxaServo[2]=pos+y-trcoxa[2];  
    CoxaServo[4]=pos-x-trcoxa[4];  

    TibiaServo[0]=pos-trtibia[0]; 
    TibiaServo[2]=pos-tbout-trtibia[2]; 
  }
  if(step==3){ 
    FemurServo[1]=pos-trfemur[1]; 
    FemurServo[3]=pos-trfemur[3]; 
    FemurServo[5]=pos-down-trfemur[5]; 
    //TibiaServo[1]=pos-trtibia[1];

    FemurServo[2]=pos-(-10)-trfemur[2]; 

  }
}
void run_leg_height1(int x, int y, int h){
  if(x>100) x=100; if(y>100) y=100;
  int pos=512, up=h, down=-20, tbin=0, tbout=-80;
  if(up>150)up=150;
  //set_speed(z); //set on serVar
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>5)step=0;
  if(step==0){
    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4];

    CoxaServo[0]=pos-y-trcoxa[0];
    CoxaServo[2]=pos-y-trcoxa[2];
    CoxaServo[4]=pos+x-trcoxa[4];
    
//    sCoxaServo[i]=speed;
//    sFemurServo[i]=speed;
    sTibiaServo[0]=specialSpeed;
    TibiaServo[0]=pos-trtibia[0];//-y*2;
    sTibiaServo[2]=specialSpeed;
    TibiaServo[2]=pos-tbin-trtibia[2];
    sTibiaServo[4]=specialSpeed;
    TibiaServo[4]=pos-tbin-trtibia[4];//-y*2;

    CoxaServo[1]=pos+y-trcoxa[1];
    CoxaServo[3]=pos-x-trcoxa[3];
    CoxaServo[5]=pos-x-trcoxa[5];

    TibiaServo[3]=pos-tbout-trtibia[3];
    TibiaServo[5]=pos-trtibia[5];
  }
  if(step==1){
        sTibiaServo[0]=specialSpeed;
    TibiaServo[0]=pos-(-20)-trtibia[0];
    sTibiaServo[2]=specialSpeed;
    TibiaServo[2]=pos-tbin-trtibia[2];
    sTibiaServo[4]=specialSpeed;
    TibiaServo[4]=pos-tbin-trtibia[4];
     count=count+20;
 }
  if(step==2){
    step++;
    FemurServo[0]=pos-down-trfemur[0];
    FemurServo[2]=pos-trfemur[2]+20;
    FemurServo[4]=pos-trfemur[4];


    FemurServo[3]=pos-(-50)-trfemur[3];

  }
  if(step==3){
    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    CoxaServo[1]=pos-y-trcoxa[1];  
    CoxaServo[3]=pos+x-trcoxa[3];  
    CoxaServo[5]=pos+x-trcoxa[5];  

    sTibiaServo[1]=specialSpeed;
    sTibiaServo[3]=specialSpeed;
    sTibiaServo[5]=specialSpeed;
    TibiaServo[1]=pos-tbin-trtibia[1];//-2*y; 
    TibiaServo[3]=pos-tbin-trtibia[3]; 
    TibiaServo[5]=pos-(-50)-trtibia[5];//-2*y; 

    CoxaServo[0]=pos+y-trcoxa[0];  
    FemurServo[2]=pos+y-20-trfemur[2];  //
    CoxaServo[2]=pos+y-trcoxa[2];  //
    CoxaServo[4]=pos-x-trcoxa[4];  

    sTibiaServo[0]=normalspeed;
    sTibiaServo[2]=normalspeed;
    sTibiaServo[4]=normalspeed;
    TibiaServo[0]=pos-trtibia[0]; 
    TibiaServo[2]=pos-tbout-trtibia[2]; 
  }
  if(step==4){ 
    step++;
    TibiaServo[1]=pos-tbin-trtibia[1]; 
    TibiaServo[3]=pos-tbin-trtibia[3]; 
    TibiaServo[5]=pos-(-50)-trtibia[5]; 
    count=count+20;
  }
  if(step==5){ 
    FemurServo[1]=pos-trfemur[1]; 
    FemurServo[3]=pos-trfemur[3]+20; 
    FemurServo[5]=pos-down-trfemur[5]; 

    FemurServo[2]=pos-(-10)-trfemur[2]; 


  }
}

void serVar(int moveSpeed, int upSpeed,int stepDelay){ //(7,moveSpeed,upSpeed,stepDelay);
  set_speed(moveSpeed);
  set_speed_leg(upSpeed);
  timer_step=stepDelay;
  mode = -1;
}

void run_leg_wall(int x, int y, int z){
  int pos=512, up=120, down=-20, tbin=0, tbout=-80;
  set_speed(z);set_speed_leg(1024);
  count++;
  if(count>=timer_step){
    step++;
    count=0;
  }
  if(step>3)step=0;
  if(step==0){
    FemurServo[0]=pos-up-trfemur[0]; 
    FemurServo[2]=pos-up-trfemur[2]; 
    FemurServo[4]=pos-up-trfemur[4];

    CoxaServo[0]=pos-y-trcoxa[0];
    CoxaServo[2]=pos-y-trcoxa[2];
    CoxaServo[4]=pos+x-trcoxa[4];

    TibiaServo[0]=pos-trtibia[0];
    TibiaServo[2]=pos-tbin-trtibia[2];

    CoxaServo[1]=pos+y-trcoxa[1];
    CoxaServo[3]=pos-x-trcoxa[3];
    CoxaServo[5]=pos-x-trcoxa[5];

    TibiaServo[3]=pos-tbout-trtibia[3];
    TibiaServo[5]=pos-trtibia[5];
  }
  if(step==1){
    FemurServo[0]=pos-down-trfemur[0];
    FemurServo[2]=pos-trfemur[2];
    FemurServo[4]=pos-trfemur[4];

    FemurServo[3]=pos-(-10)-trfemur[3];
  }
  if(step==2){
    FemurServo[1]=pos-up-trfemur[1]; 
    FemurServo[3]=pos-up-trfemur[3]; 
    FemurServo[5]=pos-up-trfemur[5]; 

    CoxaServo[1]=pos-y-trcoxa[1];  
    CoxaServo[3]=pos+x-trcoxa[3];  
    CoxaServo[5]=pos+x-trcoxa[5];  

    TibiaServo[3]=pos-tbin-trtibia[0]; 
    TibiaServo[5]=pos-(-50)-trtibia[2]; 

    CoxaServo[0]=pos+y-trcoxa[0];  
    CoxaServo[2]=pos+y-trcoxa[2];  
    CoxaServo[4]=pos-x-trcoxa[4];  

    TibiaServo[0]=pos-trtibia[0]; 
    TibiaServo[2]=pos-tbout-trtibia[2]; 
  }
  if(step==3){ 
    FemurServo[1]=pos-trfemur[1]; 
    FemurServo[3]=pos-trfemur[3]; 
    FemurServo[5]=pos-down-trfemur[5]; 

    FemurServo[2]=pos-(-10)-trfemur[2]; 

  }
}


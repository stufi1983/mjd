#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
Adafruit_BNO055 bno = Adafruit_BNO055();

#include <Pixy2SPI_SS.h>
Pixy2SPI_SS pixy;

#include <Servo.h>
Servo updown, capit;

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Mux signal pin
#define SIG_pin A4  // Analog In Pin

// Mux channel select pins (change these if required)
#define s0pin A5
#define s1pin A3
#define s2pin A1
#define s3pin A0

#define pumppin 49
#define buttonpin 32  //Tombol Hijau (start)
#define updownpin 7
#define capitpin 9
#define soundpin A6
#define keypadpin A12
#define line1pin A10
#define line2pin A8

// pin komunikasi
#define dataPin 33
#define clockPin 35
#define controlPin 37

#include "Adafruit_VL53L0X.h"

#define LOX1_ADDRESS 0x30
#define LOX2_ADDRESS 0x31
#define LOX3_ADDRESS 0x32
#define LOX4_ADDRESS 0x34
#define LOX5_ADDRESS 0x35

// set the pins to shutdown
#define SHT_LOX1 48
#define SHT_LOX2 47
#define SHT_LOX3 44
#define SHT_LOX4 46
#define SHT_LOX5 42

Adafruit_VL53L0X lox1 = Adafruit_VL53L0X();
Adafruit_VL53L0X lox2 = Adafruit_VL53L0X();
Adafruit_VL53L0X lox3 = Adafruit_VL53L0X();
Adafruit_VL53L0X lox4 = Adafruit_VL53L0X();
Adafruit_VL53L0X lox5 = Adafruit_VL53L0X();

VL53L0X_RangingMeasurementData_t measure1;
VL53L0X_RangingMeasurementData_t measure2;
VL53L0X_RangingMeasurementData_t measure3;
VL53L0X_RangingMeasurementData_t measure4;
VL53L0X_RangingMeasurementData_t measure5;

String inputString = "";      // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete


int xcounter = 0;
int flame_biner = 0;
int jarak1, jarak2, jarak3, jarak4, jarak5;
float compas;
int error, last_error, pv, LOUT, ROUT, out_p, out_d;
int sp = 12;
int kp = 8;
int kd = 20;

const int peka_flame = 800;
const int set_point_cam = 150;

float baca_bno() {
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  float sudut = euler.x();
  float sudut_ok;
  if (sudut >= 0 && sudut < 180) {
    sudut_ok = sudut;
  } else {
    sudut_ok = 0 - (360 - sudut);
  }
  return sudut_ok;
}

float baca_bno_tinggi() {
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  float sudut = euler.z();
  float sudut_ok;
  if (sudut >= 0 && sudut < 180) {
    sudut_ok = sudut;
  } else {
    sudut_ok = 0 - (360 - sudut);
  }
  return sudut_ok;
}

// rumus pid
void out_pid() {
  out_p = (int)error * kp;
  out_d = (int)(error - last_error) * kd;
  last_error = error;
}

// kosongkan nilai pid
void clear_pid() {
  error = 0;
  last_error = 0;
  pv = 0;
  out_p = 0;
  out_d = 0;
  LOUT = 0;
  ROUT = 0;
}

void VL53L0X_init() {
  // all reset
  digitalWrite(SHT_LOX1, LOW);
  digitalWrite(SHT_LOX2, LOW);
  digitalWrite(SHT_LOX3, LOW);
  digitalWrite(SHT_LOX4, LOW);
  digitalWrite(SHT_LOX5, LOW);
  delay(50);
  // all unreset
  digitalWrite(SHT_LOX1, HIGH);
  digitalWrite(SHT_LOX2, HIGH);
  digitalWrite(SHT_LOX3, HIGH);
  digitalWrite(SHT_LOX4, HIGH);
  digitalWrite(SHT_LOX5, HIGH);
  delay(50);

  digitalWrite(SHT_LOX1, HIGH);
  digitalWrite(SHT_LOX2, LOW);
  digitalWrite(SHT_LOX3, LOW);
  digitalWrite(SHT_LOX4, LOW);
  digitalWrite(SHT_LOX5, LOW);

  if (!lox1.begin(LOX1_ADDRESS)) {
    Serial.println("SENSOR 1 ERROR");
    while (1)
      ;
  }
  delay(50);

  digitalWrite(SHT_LOX2, HIGH);
  if (!lox2.begin(LOX2_ADDRESS)) {
    Serial.println("SENSOR 2 ERROR");
    while (1)
      ;
  }
  delay(50);

  digitalWrite(SHT_LOX3, HIGH);
  if (!lox3.begin(LOX3_ADDRESS)) {
    Serial.println("SENSOR 3 ERROR");
    while (1)
      ;
  }
  delay(50);

  digitalWrite(SHT_LOX4, HIGH);
  if (!lox4.begin(LOX4_ADDRESS)) {
    Serial.println("SENSOR 4 ERROR");
    while (1)
      ;
  }
  delay(50);

  digitalWrite(SHT_LOX5, HIGH);
  if (!lox5.begin(LOX5_ADDRESS)) {
    Serial.println("SENSOR 5 ERROR");
    while (1)
      ;
  }
  delay(50);
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

void read_sensor() {
  lox1.rangingTest(&measure1, false);
  lox2.rangingTest(&measure2, false);
  lox3.rangingTest(&measure3, false);
  lox4.rangingTest(&measure4, false);
  lox5.rangingTest(&measure5, false);
  jarak1 = (float)measure1.RangeMilliMeter / 10;
  jarak2 = (float)measure2.RangeMilliMeter / 10;
  jarak3 = (float)measure3.RangeMilliMeter / 10;
  jarak4 = (float)measure4.RangeMilliMeter / 10;
  jarak5 = (float)measure5.RangeMilliMeter / 10;
}

void sendData(byte data) {
  for (int i = 0; i < 8; i++) {
    bool dataout = data & (1 << i);
    digitalWrite(dataPin, dataout);
    digitalWrite(clockPin, 1);
    digitalWrite(clockPin, 0);
    delayMicroseconds(1);
  }
}

void sendall(byte mode, int x, int y, int z) {
  digitalWrite(controlPin, 1);
  sendData((byte)mode);
  sendData((int)(x >> 8) & 0xFF);  //untuk memecah nilai bit 16bit
  sendData((int)x & 0xFF);
  sendData((int)(y >> 8) & 0xFF);
  sendData((int)y & 0xFF);
  sendData((int)(z >> 8) & 0xFF);
  sendData((int)z & 0xFF);
  digitalWrite(controlPin, 0);
}

void semprot_on() {
  digitalWrite(pumppin, 1);
}

void semprot_off() {
  digitalWrite(pumppin, 0);
}

byte key_next() {
  int adc = analogRead(keypadpin);
  if (adc > 970 && adc < 1000) return 0;
  else
    return 1;
}

byte key_back() {
  int adc = analogRead(keypadpin);
  if (adc > 900 && adc < 950) return 0;
  else
    return 1;
}

byte key_up() {
  int adc = analogRead(keypadpin);
  if (adc > 650 && adc < 750) return 0;
  else
    return 1;
}

byte key_down() {
  int adc = analogRead(keypadpin);
  if (adc > 450 && adc < 550) return 0;
  else
    return 1;
}

byte key_exit() {
  int adc = analogRead(keypadpin);
  if (adc > 1000) return 0;
  else
    return 1;
}


int readMux(int channel) {
  int cPin[] = { s0pin, s1pin, s2pin, s3pin };
  int muxChannel[16][4] = {
    { 0, 0, 0, 0 },  //channel 0
    { 1, 0, 0, 0 },  //channel 1
    { 0, 1, 0, 0 },  //channel 2
    { 1, 1, 0, 0 },  //channel 3
    { 0, 0, 1, 0 },  //channel 4
    { 1, 0, 1, 0 },  //channel 5
    { 0, 1, 1, 0 },  //channel 6
    { 1, 1, 1, 0 },  //channel 7
    { 0, 0, 0, 1 },  //channel 8
    { 1, 0, 0, 1 },  //channel 9
    { 0, 1, 0, 1 },  //channel 10
    { 1, 1, 0, 1 },  //channel 11
    { 0, 0, 1, 1 },  //channel 12
    { 1, 0, 1, 1 },  //channel 13
    { 0, 1, 1, 1 },  //channel 14
    { 1, 1, 1, 1 }   //channel 15
  };
  //loop through the 4 sig
  for (int i = 0; i < 4; i++) {
    digitalWrite(cPin[i], muxChannel[channel][i]);
  }

  //read the value at the SIG pin
  int val = analogRead(SIG_pin);
  return val;
}
unsigned int read_flame() {
  unsigned int output = 0;
  unsigned int penjumlah[] = { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
  for (int i = 0; i < 16; i++) {
    int adc = readMux(i);
    //Serial.print(adc);
    //Serial.print(" ");
    lcd.setCursor(i, 1);
    if (adc < peka_flame) {
      output = output + penjumlah[i];
      lcd.write('1');
    } else {
      lcd.write('0');
    }
  }
  //Serial.println();
  return output;
}

unsigned long previousMillis = 0;
const long interval = 1000;

volatile int counter = 0;
void ISRext() {
  counter++;
}

void read_uvtron() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    counter = 0;
  }
}

void reset_bno() {
  if (!bno.begin()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("BNO055 ERROR!");
    delay(1000);
  }
}

void chek_hardware() {
  lcd.clear();
  delay(500);
  int menu = 0;
  int indek = 0;

  while (1) {

    if (key_next() == 0) menu++;
    if (key_back() == 0) menu--;
    if (key_exit() == 0) break;

    if (menu > 7) menu = 0;
    if (menu < 0) menu = 7;


    if (menu == 0) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Jarak:");
      lcd.print(jarak1);
      lcd.print(" ");
      lcd.print(jarak2);
      lcd.setCursor(0, 1);
      lcd.print(jarak3);
      lcd.print(" ");
      lcd.print(jarak4);
      lcd.print(" ");
      lcd.print(jarak5);
      read_sensor();
    }

    if (menu == 1) {
      read_uvtron();
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Uvtron:");
      lcd.print(counter);
    }

    if (menu == 2) {
      int data_flame = readMux(indek);

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Flame");
      lcd.print(indek);
      lcd.print(": ");
      lcd.print(data_flame);
      if (key_up() == 0) indek++;
      if (key_down() == 0) indek--;
      if (indek > 15) indek = 0;
      if (indek < 0) indek = 15;
      read_flame();
    }

    if (menu == 3) {
      pixy.ccc.getBlocks();
      int sig = 0, datax = 0, datay = 0;
      if (pixy.ccc.numBlocks) {
        sig = pixy.ccc.blocks[0].m_signature;
        datax = pixy.ccc.blocks[0].m_x;
        datay = pixy.ccc.blocks[0].m_y;
      }

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".S");
      lcd.print(sig);
      lcd.print(" X");
      lcd.print(datax);
      lcd.print(" Y");
      lcd.print(datay);
      lcd.setCursor(0, 1);
      lcd.print("SP:");
      lcd.print(datax - set_point_cam);
      char buffer[40];
      sprintf(buffer, "s%d x%d y%d w%d h%d a%d i%d e%d ",
              pixy.ccc.blocks[0].m_signature,
              pixy.ccc.blocks[0].m_x,
              pixy.ccc.blocks[0].m_y,
              pixy.ccc.blocks[0].m_width,
              pixy.ccc.blocks[0].m_height,
              pixy.ccc.blocks[0].m_angle,
              pixy.ccc.blocks[0].m_index,
              pixy.ccc.blocks[0].m_age);
      Serial.println(buffer);
    }

    if (menu == 4) {
      compas = baca_bno();
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".COMPAS: ");
      lcd.print(compas);

      if (key_up() == 0) reset_bno();
    }

    if (menu == 5) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Line1: ");
      lcd.print(analogRead(line1pin));
      lcd.setCursor(0, 1);
      lcd.print("  Line2: ");
      lcd.print(analogRead(line2pin));
    }

    if (menu == 6) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Sound: ");
      lcd.print(analogRead(soundpin));
    }

    if (menu == 7) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(menu);
      lcd.print(".Semprot: ");
      lcd.print(digitalRead(pumppin));
      if (key_up() == 0) semprot_on();
      else
        semprot_off();
    }

    delay(200);
  }
}

void kanan_auto(int speed) {
  lcd.backlight();
  while (1) {
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print("BELOK KANAN");
    sendall(2, 50, -50, speed);  // jalan
    read_sensor();
    if (jarak2 > 20 && jarak3 > 30) break;
  }
  //lcd.noBacklight();
  delay(200);
}

void kiri_auto(int speed) {
  lcd.backlight();
  while (1) {
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print("BELOK KIRI");
    sendall(2, -50, 50, speed);  // jalan
    read_sensor();
    if (jarak4 > 20 && jarak3 > 30) break;
  }
  //lcd.noBacklight();
  delay(200);
}

void wall_kiri(int speed, int s_sp, int skp, int skd, int xtimer, int setjarak, int uvtrondetect) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {
    int j2 = jarak2 - 13;
    pv = min(jarak1, j2);
    error = pv - sp;
    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Timer: ");
    lcd.print(cnttimer);
    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);
    sendall(1, LOUT, ROUT, speed);  // jalan
    read_sensor();
    if (cnttimer < xtimer) {
      cnttimer++;
      lcd.backlight();
    } else {
      //lcd.noBacklight();
    }

    if (uvtrondetect == 1) {
      if (jarak3 < setjarak && cnttimer >= xtimer && counter > 0) break;
      read_uvtron();
    } else {
      if (setjarak > 0) {
        if (jarak3 < setjarak && cnttimer >= xtimer) break;
      } else {
        if (cnttimer >= xtimer) break;
      }
    }
  }
}

void wall_kanan(int speed, int s_sp, int skp, int skd, int xtimer, int setjarak, int uvtrondetect) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {
    int j4 = jarak4 - 13;
    pv = min(j4, jarak5);
    error = sp - pv;
    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Timer: ");
    lcd.print(cnttimer);
    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);
    // sendall(1, LOUT, ROUT, speed);  // jalan
    sendall(8, LOUT, ROUT, speed);  // jalan
    read_sensor();
    if (cnttimer < xtimer) {
      cnttimer++;
      lcd.backlight();
    } else {
      //lcd.noBacklight();
    }


    if (uvtrondetect == 1) {
      if (jarak3 < setjarak && cnttimer >= xtimer && counter > 0) break;
      read_uvtron();
    } else {
      if (setjarak > 0) {
        if (jarak3 < setjarak && cnttimer >= xtimer) break;
      } else {
        if (cnttimer >= xtimer) break;
      }
    }
  }
}

void standby() {
  lcd.backlight();
  while (1) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(digitalRead(buttonpin));
    lcd.print(" ");
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(jarak2);
    lcd.print(" ");
    lcd.print(jarak3);
    lcd.setCursor(0, 1);
    lcd.print(" ");
    lcd.print(jarak4);
    lcd.print(" ");
    lcd.print(jarak5);
    lcd.print(" ");
    float sudut = baca_bno_tinggi();
    lcd.print(sudut);
    read_sensor();
    char buffer[40];
    sprintf(buffer, "standby %d %d %d %d %d ", jarak1, jarak2, jarak3, jarak4, jarak5);
    Serial.print(buffer);
    Serial.println(sudut);

    if (key_next() == 0) {
      chek_hardware();
    }



    if (digitalRead(buttonpin) == 0) {
      break;
    }
  }
}

void stop_permanent() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Stop");
  while (1) {
    sendall(0, 0, 0, 500);
    delay(500);
    //lcd.noBacklight();
    delay(500);
    lcd.backlight();
  }
}

void stop_delay(unsigned int tm) {
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Stop: ");
    lcd.print(i);
    sendall(0, 0, 0, 500);
    delay(1);
  }
  //lcd.noBacklight();
  delay(200);
}

void kanan_delay(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Kanan: ");
    lcd.print(i);
    sendall(2, 50, -50, 500);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void geser_kanan(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("G Kanan: ");
    lcd.print(i);
    sendall(4, 0, 0, 300);  // geser kiri
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void maju_delay(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Maju: ");
    lcd.print(i);
    sendall(1, 50, 50, 300);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void maju_delaya(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Maju: ");
    lcd.print(i);
    sendall(1, 10, 10, 150);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void mundur_delay(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Mundur: ");
    lcd.print(i);
    sendall(1, -50, -50, 300);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void mundur_delaya(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Mundur: ");
    lcd.print(i);
    sendall(1, -10, -10, 150);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}



void kiri_delay(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("Kiri: ");
    lcd.print(i);
    sendall(2, -50, 50, 500);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void geser_kiri(unsigned int tm) {
  lcd.clear();
  lcd.backlight();
  for (unsigned int i = 0; i < tm; i++) {
    lcd.setCursor(0, 0);
    lcd.print("G Kiri: ");
    lcd.print(i);
    sendall(3, 0, 0, 300);  // jalan
    delay(1);
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void mode_start1() {

  //   sendall(byte mode, int langkah_sudut_kiri, int langkah_sudut_kanan, int kecepatan)
  //  mode: 0 diam, 1 maju/mundur, 2 mutar, 3 geser kiri, 4 geser kanan
  //Jarak Kiri,kanan < 100mm, depan <50mm
  if (jarak1 < 100 && jarak5 > 100 && jarak3 < 50) {
    for (int i = 0; i < 10; i++) sendall(2, 50, -50, 500);  ///muter kanan
  }
  //Jarak Kiri > 100mm, Kanan <100 depan <50mm
  if (jarak1 > 100 && jarak5 < 100 && jarak3 < 50) {
    for (int i = 0; i < 10; i++) sendall(2, -50, 50, 500);  ///muter kiri
  }
  if (jarak1 < 25 && jarak5 < 25 && jarak3 < 90) {
    for (int i = 0; i < 10; i++) sendall(2, 50, -50, 500);  ///muter kanan
  }
  lcd.backlight();
  while (1) {

    if (jarak1 < 25 && jarak5 < 25) {
      if (jarak3 > 400) break;
    }

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(jarak2);
    lcd.setCursor(0, 1);
    lcd.print(jarak3);
    lcd.print(" ");
    lcd.print(jarak4);
    lcd.print(" ");
    lcd.print(jarak5);
    read_sensor();
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void padamkan_api(int xdelay) {
  lcd.backlight();
  while (1) {

    if ((flame_biner & 0b0000000001111111) > 0) sendall(2, 50, -50, 500);
    if ((flame_biner & 0b1111111000000000) > 0) sendall(2, -50, 50, 500);
    flame_biner = read_flame();
    if ((flame_biner & 0b0000000110000000) > 0) {
      for (int i = 0; i < 10; i++) sendall(5, 100, 100, 500);
      semprot_on();
      delay(xdelay);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("DONE!");
      delay(1000);
      semprot_off();
      break;
    }
  }
  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
}

void kompas_kiri(float set_comp) {
  lcd.backlight();
  lcd.clear();

  while (1) {
    sendall(2, -20, 20, 300);
    compas = baca_bno();
    lcd.setCursor(0, 0);
    lcd.print("COMPAS KIRI: ");
    lcd.setCursor(0, 1);
    lcd.print(compas);

    if (compas <= set_comp) break;
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
}

void kompas_kanan(float set_comp) {
  lcd.backlight();
  lcd.clear();

  while (1) {
    sendall(2, 20, -20, 300);
    compas = baca_bno();
    lcd.setCursor(0, 0);
    lcd.print("COMPAS KANAN: ");
    lcd.setCursor(0, 1);
    lcd.print(compas);

    if (compas >= set_comp) break;
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
}


//Geser Kiri
//While(x)

int posisi_boneka_kiri(int timr) {
  lcd.backlight();
  lcd.clear();
  int sig = 0, datax = 150, point_ok;
  int timerx = 0;
  int bonekaok = 0;
  while (1) {
    sendall(3, 50, 0, 250);

    pixy.ccc.getBlocks();

    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datax = pixy.ccc.blocks[0].m_x;
      lcd.clear();
    }

    point_ok = datax - set_point_cam;

    if (point_ok > -10 && point_ok < 10 && sig == 1) {
      bonekaok = 1;
      break;
    }

    if (timerx > timr) {
      bonekaok = 0;
      break;
    }


    lcd.setCursor(0, 0);
    lcd.print("KIRI: ");
    lcd.print(sig);
    lcd.print(" ");
    lcd.print(datax);
    lcd.setCursor(0, 1);
    lcd.print("SpCam: ");
    lcd.print(point_ok);

    timerx++;
    delay(1);
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);

  return bonekaok;
}

int posisi_boneka_kanan(int timr) {
  lcd.backlight();
  lcd.clear();
  int sig = 0, datax = -150, point_ok;
  int timerx = 0;
  int bonekaok = 0;
  while (1) {
    sendall(4, 0, 0, 100);

    pixy.ccc.getBlocks();

    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datax = pixy.ccc.blocks[0].m_x;
      lcd.clear();
    }

    point_ok = datax - set_point_cam;

    if (point_ok > -10 && point_ok < 10 && sig == 1) {
      bonekaok = 1;
      break;
    }

    if (timerx > timr) {
      bonekaok = 0;
      break;
    }

    lcd.setCursor(0, 0);
    lcd.print("KANAN: ");
    lcd.print(sig);
    lcd.print(" ");
    lcd.print(datax);
    lcd.setCursor(0, 1);
    lcd.print("SpCam: ");
    lcd.print(point_ok);

    timerx++;

    delay(1);
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  return bonekaok;
}

void standby_gripper() {
  updown.write(90);
  capit.write(30);
}

void turun_gripper() {  //// jalan mendekati korban
  updown.write(0);
}
void naik_gripper() {  //// standby
  updown.write(0);
}
void capit_gripper() {  //// capit korban
  capit.write(160);
}
void capit_grippera() {  //// capit korban
  capit.write(150);
}
void lepas_gripper() {  //// standby/lepas korban
  capit.write(100);
}


void ambil_boneka() {
  lcd.backlight();
  lcd.clear();
  int sig = 0, datax = 150, datay = 150;

  lepas_gripper();
  delay(10);
  lepas_gripper();
  delay(10);
  turun_gripper();
  delay(20);
  turun_gripper();
  delay(20);

  while (1) {
    sendall(1, 30, 30, 150);

    pixy.ccc.getBlocks();

    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datax = pixy.ccc.blocks[0].m_x;
      datay = pixy.ccc.blocks[0].m_y;
      lcd.clear();
    }

    if (sig == 1 && datay >= 190) break;


    lcd.setCursor(0, 0);
    lcd.print("AMBIL: ");
    lcd.print(sig);

    lcd.setCursor(0, 1);
    lcd.print("X");
    lcd.print(datax);
    lcd.print(" Y");
    lcd.print(datay);
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  maju_delaya(100);

  capit_grippera();
  delay(500);
  capit_gripper();
  delay(500);
  capit_gripper();
  delay(500);
  naik_gripper();
  delay(500);
  naik_gripper();
  delay(500);
  naik_gripper();
  delay(1000);
}

void mode_start2() {

  // start rute tercepat
  if (jarak1 < 100 && jarak5 > 100 && jarak3 < 100) {
    for (int i = 0; i < 10; i++) sendall(2, -50, 50, 500);
  }
  if (jarak1 > 100 && jarak5 < 100 && jarak3 < 100) {
    for (int i = 0; i < 10; i++) sendall(2, 50, -50, 500);
  }
  if (jarak1 < 25 && jarak5 < 25 && jarak3 > 100) {
    for (int i = 0; i < 10; i++) sendall(2, 50, -50, 500);
  }
  lcd.backlight();
  while (1) {

    // start rute tercepat
    if (jarak1 < 22 && jarak5 < 22) {
      if (jarak3 < 100 && jarak3 > 50) break;
    }

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(jarak2);
    lcd.setCursor(0, 1);
    lcd.print(jarak3);
    lcd.print(" ");
    lcd.print(jarak4);
    lcd.print(" ");
    lcd.print(jarak5);
    read_sensor();
  }

  for (int i = 0; i < 10; i++) sendall(0, 0, 0, 500);
  //lcd.noBacklight();
  delay(200);
}

void mode_start3() {
  if (jarak1 >  jarak5 && jarak3 < 50) {

    kiri_delay(65);

    goto next;
  }
  if (jarak1 < jarak5 && jarak3 < 50) {

    kanan_delay(65);

  }

next:
  delay(5);
}



//0 = CERMIN JAUH//
void mode_start4() {
  if (jarak1 < 100 && jarak5 > 100 && jarak3 < 50) {
    kompas_kanan(90.0);
    delay(1000);
    reset_bno();
    delay(1000);
  }

  if (jarak1 > 100 && jarak5 < 100 && jarak3 < 50) {
    kompas_kanan(175.0);
    delay(1000);
    reset_bno();
    delay(1000);
    kompas_kanan(90.0);
    goto next;
  }

next:
  delay(500);
}


void setup() {
  sendall(0, 0, 0, 500);  //diam

  //lcd.begin();
  lcd.init();

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("STARTING");
  inputString.reserve(200);
  Serial.begin(115200);
  Serial.println("STARTING");

  updown.attach(updownpin);
  capit.attach(capitpin);

  standby_gripper();

  //all pins are outputs
  pinMode(s0pin, OUTPUT);
  pinMode(s1pin, OUTPUT);
  pinMode(s2pin, OUTPUT);
  pinMode(s3pin, OUTPUT);

  pinMode(pumppin, OUTPUT);
  pinMode(buttonpin, INPUT_PULLUP);

  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(controlPin, OUTPUT);

  digitalWrite(dataPin, 0);
  digitalWrite(clockPin, 0);
  digitalWrite(controlPin, 0);

  pinMode(SHT_LOX1, OUTPUT);
  pinMode(SHT_LOX2, OUTPUT);
  pinMode(SHT_LOX3, OUTPUT);
  pinMode(SHT_LOX4, OUTPUT);
  pinMode(SHT_LOX5, OUTPUT);

  VL53L0X_init();

  pixy.init();

  if (!bno.begin()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("BNO055 ERROR!");
    delay(1000);
  }
  bno.setExtCrystalUse(true);


  Serial.println("READY");

  clear_pid();

  pinMode(3, INPUT);
  attachInterrupt(digitalPinToInterrupt(3), ISRext, RISING);

  //   sendall(byte mode, int langkah_sudut_kiri, int langkah_sudut_kanan, int kecepatan)
  //  mode: 0 diam, 1 maju/mundur, 2 mutar, 3 geser kiri, 4 geser kanan

  //Baca sensor (baca_bno): 0 pertama kali aktif, kanan positif, kiri negatif

  sendall(0, 0, 0, 500);  //diam
}

float sudut;
const float sudutPutar = -45;


void putarKiri(int kecepatan, int tinggi, int sudutPutar) {
  if (sudutPutar > 0) sudutPutar = sudutPutar * -1;
  int sudutNow = baca_bno();
  int batas = sudutNow + sudutPutar;
  if (batas < -180) batas += 360;
  Serial.println(batas);
  if ((sudutNow + sudutPutar) < -180) {
    do {
      //berputar ke kiri
      sendall(2, -1 * kecepatan, kecepatan, tinggi);
      sudut = baca_bno();
      Serial.print("PutarKiri ");
      Serial.print(sudut);
      Serial.print(":");
      Serial.println(batas);

    } while (sudut <= 0);
  }

  do {
    sudut = baca_bno();
    //berputar ke kiri
    sendall(2, -1 * kecepatan, kecepatan, tinggi);
    Serial.print("PutarKiri ");
    Serial.print(sudut);
    Serial.print(":");
    Serial.println(batas);
  } while (sudut > batas);
}

void putarKanan(int sudutKaki, int kecepatan, int sudutPutar) {
  int sudutNow = baca_bno();
  int batas = sudutNow + sudutPutar;
  if (batas > 180) batas -= 360;
  Serial.println(batas);
  if ((sudutNow + sudutPutar) > 180) {
    do {
      //berputar ke kanan
      sendall(2, sudutKaki, -1 * sudutKaki, kecepatan);
      sudut = baca_bno();
      Serial.print("PutarKanan ");
      Serial.print(sudut);
      Serial.print(":");
      Serial.println(batas);

    } while (sudut > 0);
  }

  do {
    sudut = baca_bno();
    //berputar ke kanan
    sendall(2, sudutKaki, -1 * sudutKaki, kecepatan);
    Serial.print("PutarKanan ");
    Serial.print(sudut);
    Serial.print(":");
    Serial.println(batas);
  } while (sudut < batas);
}

void majuEasyT(int kecepatan, int jarak_langkah, int tinggi_kaki, int timer) {
  //maju ke depan
  sendall(1, kecepatan, kecepatan, jarak_langkah);
  sendall(6, kecepatan, kecepatan, tinggi_kaki);
  int t = 0;
  do {
    read_sensor();
    char buffer[40];
    sprintf(buffer, "Jarak %d %d %d %d %d %d", jarak1, jarak2, jarak3, jarak4, jarak5, t);
    Serial.println(buffer);
    delay(1000);
    t++;
  } while (t < timer);
}

void majuEasy(int kecepatan, int jarak_langkah, int tinggi_kaki, int batas_jarak) {
  //maju ke depan
  sendall(1, kecepatan, kecepatan, jarak_langkah);
  sendall(6, kecepatan, kecepatan, tinggi_kaki);
  do {
    read_sensor();
    char buffer[40];
    sprintf(buffer, "Jarak %d %d %d %d %d ", jarak1, jarak2, jarak3, jarak4, jarak5);
    Serial.println(buffer);
  } while (jarak3 > batas_jarak);
}

void majuEasy(int kecepatan, int jarak_langkah, int batas_jarak) {
  //maju ke depan
  sendall(1, kecepatan, kecepatan, jarak_langkah);
  do {
    read_sensor();
    char buffer[40];
    sprintf(buffer, "Jarak %d %d %d %d %d ", jarak1, jarak2, jarak3, jarak4, jarak5);
    Serial.println(buffer);
  } while (jarak3 > batas_jarak);
}

bool apiTerdeteksi = false;

void majuCariApi(int kecepatan, int tinggi, int batas_jarak) {
  //maju ke depan
  sendall(1, kecepatan, kecepatan, tinggi);
  do {
    read_sensor();
    cekApi();
    char buffer[40];
    sprintf(buffer, "Jarak %d %d %d %d %d ", jarak1, jarak2, jarak3, jarak4, jarak5);
    Serial.println(buffer);
  } while (jarak3 > batas_jarak || !apiTerdeteksi);
}



void cekApi() {
  flame_biner = read_flame();
  if ((flame_biner & 0b1111111111111111) > 0) {
    apiTerdeteksi = true;
  }
}
void cariBonekaPixy(int maksimalSudutPencarian, int langkahKaki, int jarakHenti_Y, int offsetKananKiri) {
  //Cari boneka
  int sig = 0, datax = 0, datay = 0;
  lcd.clear();
  reset_bno();

  bool adaBoneka = false;
  bool bonekaKanan = true;

  //Jika tidak ada boneka terdeteksi, cari ke kanan
  lcd.setCursor(14, 1);
  lcd.print(">>");
  do {
    pixy.ccc.getBlocks();
    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datax = pixy.ccc.blocks[0].m_x;
      if (sig == 1) {
        adaBoneka = true;  //bonekaterdeteksi
        break;
      }
    }
    lcd.setCursor(0, 0);
    lcd.print(sig); lcd.print(" ");
    lcd.print(datax); lcd.print(" ");
    sendall(2, langkahKaki, -1 * langkahKaki, 500); // muter kanan pelan
  } while (baca_bno() < maksimalSudutPencarian);

  //Jika tidak ada boneka terdeteksi di kanan, cari ke kiri
  if (!adaBoneka) {
    lcd.setCursor(14, 1);
    lcd.print("<<");
    do {
      pixy.ccc.getBlocks();
      if (pixy.ccc.numBlocks) {
        sig = pixy.ccc.blocks[0].m_signature;
        datax = pixy.ccc.blocks[0].m_x;
        if (sig == 1) {
          adaBoneka = true;  //bonekaterdeteksi
          break;
        }
      }
      lcd.setCursor(0, 0);
      lcd.print(sig); lcd.print(" ");
      lcd.print(datax); lcd.print(" ");
      sendall(2, -1 * langkahKaki, langkahKaki, 500); // muter kiri pelan
    } while (baca_bno() > (maksimalSudutPencarian * -1));
    bonekaKanan = false;
  }

  //jika tidak ada boneka, lanjutkan misi berikutnya
  if (!adaBoneka) return;

  //jika ada boneka, cari posisi boneka berdasar nilai x
  sendall(7, 500, 1024, 30);
  lcd.setCursor(14, 1);
  if (datax < 150 && datax > 0) {
    lcd.print("B<");
    LOUT = -1 * langkahKaki; ROUT = langkahKaki;
  } else   if (datax > 150) {
    lcd.print("B>");
    LOUT = langkahKaki; ROUT = -1 * langkahKaki;
  }

  //pusatkan ke arah boneka
  do {
    lcd.setCursor(5, 1);
    lcd.print("<>");
    sendall(2, LOUT, ROUT, 600);  // putar
    pixy.ccc.getBlocks();
    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datax = pixy.ccc.blocks[0].m_x;
    } else {
      sendall(6, langkahKaki, langkahKaki, 120);  // maju
      delay(5);
    }
    lcd.print(datax);
  } while ((datax - set_point_cam) > (2 * offsetKananKiri) || (datax - set_point_cam) < (-2 * offsetKananKiri)); //(datax<150-(2*offsetKananKiri) || datax>150+(2*offsetKananKiri));//datax - set_point_cam>0);

  //maju ke arah boneka
  do {
    pixy.ccc.getBlocks();
    if (pixy.ccc.numBlocks) {
      sig = pixy.ccc.blocks[0].m_signature;
      datay = pixy.ccc.blocks[0].m_y;
      datax = pixy.ccc.blocks[0].m_x;
      lcd.clear();
      lcd.setCursor(5, 1);
      lcd.print("MP Y:");
      lcd.print(datay);
      int last_datax = datax;
      int m_width = pixy.ccc.blocks[0].m_width;
      int space = 316 - m_width;
      if (space <= 0) space = offsetKananKiri;
      int center = space / 2;
      if (datax < (center - offsetKananKiri)) {
        LOUT = 10;
        ROUT = 20;
      } else {
        LOUT = 20;
        ROUT = 10;
      }
      if (sig == 1) {
        sendall(6, LOUT, ROUT, 100);  // maju
        delay(10);
      } else {
        if (last_datax > 150) { //Kanan luar jangkauan
          LOUT = 0; ROUT = -1 * langkahKaki;
        } else if (last_datax < 150) { //Kiri luar jangkauan
          LOUT = -1 * langkahKaki; ROUT = 0;
        }
        sendall(6, LOUT, ROUT, 100);  // putar
        delay(10);
      }
      if (datax > 1 && datax < 300) {
        last_datax = datax;
      }
    }
    //sendall(6, 0, 0, 100);
  } while (datay < jarakHenti_Y);
  sendall(0, 0, 0, 100);  // diam
}

#define jarakApiUVTron 5
void loop() {
  standby();


  //------ Maju lorong 1 -------------
  mode_start3();    //start
  lcd.setCursor(14, 1);
  lcd.print("MP");
  MajuPID(350, 10, 5, 3, 25);                                       // maju lorong 1

  //------ Belok kiri ke 1 -------------
  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(70, 200, -60);                                          //belok kiri setelah lorong 1


  lcd.setCursor(14, 1);                                             //kalibrasi sudut lewat jarak, tikungan 1
  lcd.print("KK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri
    delay(10);
    read_sensor();
  } while (jarak1 < 800 && jarak3 < 100);
  sendall(0, 0, 0, 512);  //berhenti sejenak

  //------ Maju rintangan puing random 1 -------------
  reset_bno();
  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(200, 1024, 10, 60, 90, 20, 200);                              //lewat puing-puing 1
  if (baca_bno() < -85) goto naikNanjak;

  //------ Putar kiri sebelum naik -------------
  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(90, 200, -50);                                           //putar kiri setelah puing 1


  lcd.setCursor(14, 1);                                               //kalibrasi sudut lewat jarak, tikungan 2
  lcd.print("KK");
  do {
    read_sensor();
    sendall(2, -10, 10, 500);  // muter kiri pelan
    delay(10);
  } while (jarak1 < 100);

  sendall(0, 0, 0, 512);  //berhenti dan reset bno

naikNanjak:
  //------ Jalan Naik   -------------
  reset_bno();
  lcd.setCursor(14, 1);
  lcd.print("NK");
  MajuNaikKompas(100, 250, 20, 70, 100, 60);                       //Naik Tanjakan

  //------ Jalan Turun   -------------

  lcd.setCursor(14, 1);
  lcd.print("TK");
  MajuTurunKompas(96, 1024, 15, 20, 80, 55);                           //Turunan

  

  //kalibrasi hingga selesai turun lewat jarak depan
  lcd.setCursor(14, 1);
  lcd.print("KT");
  do {
    sendall(6, 50, 50, 85);
    delay(10);
    read_sensor();
  } while (jarak3 > 65 && jarak3 < 800);

  

  //------ Putar kiri arah home   -------------

  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(50, 200, -70);                                            //Putar kiri setelah Turunan

  lcd.setCursor(14, 1);                                               //kalibrasi sudut lewat jarak
  lcd.print("KK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri pelan
    delay(10);
    read_sensor();
  } while (jarak1 < 60 || (jarak3 < 110 && jarak3 > 800));

  sendall(0, 0, 0, 512);  //berhenti dan reset bno
  reset_bno();

  //------ Maju sedikit ke arah belokan puing   -------------
  lcd.setCursor(14, 1);
  lcd.print("MK");
  do {
    MajuKompas(240, 512, 5, 50, 35, 75);                                // Jalan sampai depan ruang 1
    sendall(1, 50, 50, 240);
  } while (jarak1 < 100);
  sendall(0, 0, 0, 512);
  //------ Putar kiri ke arah puing random   -------------

  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(70, 200, -70);


  lcd.setCursor(14, 1);                                           //kalibrasi sudut lewat jarak depan dan kiri
  lcd.print("KK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri
    delay(10);
    read_sensor();
  } while (jarak1 < 800 || jarak3 < 100);


  reset_bno();

  //------ Melewati puing random dekat lilin   -------------
  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(240, 1024, 10, 60, 90, 40);                               //lewat puing 2

  sendall(0, 0, 0, 512);  //berhenti


  //////////////////////PADAMKAN API///////////////////
  padamkan_api(3000);

  ////////////////////////ANGKAT KORBAN////////////////
  //Tutup bagian ini jika tidak mengangkat korban
  //cariBonekaPixy(20, 30, 185, 3); //sudut cari 20 ke kanan dan kekiri, langkah kaki 15, batas pixy Y 185, offset 3 pixel
  //angkatKorban(0, 0, 48, 60); //void angkatKorban(int turun, int buka, int ncapit, int naik) {
  //delay(1000);
  ////////////////////////////////////////////////////




  //  lcd.setCursor(14, 1);
  //  lcd.print("PK");
  //  putarKiri(70, 200, -150);                                            //HADAP SAFETY ZONE

  //kalibrasi arah safetyzone
  lcd.setCursor(14, 1);
  lcd.print("KK");
  do {
    sendall(2, -30, 30, 500);  // muter kiri
    delay(10);
    read_sensor();
  } while (jarak3 < 128);

  reset_bno();
  //maju rintangan random 2
  lcd.setCursor(14, 1);
  lcd.print("MK");
  do {
    MajuKompas(240, 1024, 10, 60, 90, 63);                               //lewat puing 2  OTW SAFETY ZONE
    read_sensor();
    sendall(1, 20, 20, 500);
    if (baca_bno() > 30) {
      sendall(1, 0, 20, 500);
    }
    delay(10);
  } while (jarak1 < 33 || jarak5 < 33);

  /////taruh korban////
 // sendall(0, 0, 0, 500);
 // taruhKorban(60, 0, 0);
 // standby_gripper();
 // delay(2000);
  /////////////////////

  lcd.setCursor(14, 1);
  lcd.print("PK");
  reset_bno();
  putarKanan(50, 500, 80);                                            //HADAP tanpa wall

  //kalibrasi arah tanpa wall
  sendall(7, 1000, 1000, 5);  //spd,legspd,tmrStep
  lcd.setCursor(14, 1);
  lcd.print("KK");
  do {
    sendall(2, 20, -20, 500);  // muter kanan
    delay(10);
    read_sensor();
    if (baca_bno() > 95 && baca_bno < 130)break;
  } while (jarak3 < 800 || jarak5 < 100);

  read_sensor();
  while (jarak1 < 60) {
    sendall(4, 0, 0, 200);
    delay(5);
    read_sensor();
  }

  ///===============
  sendall(7, 500, 500, 15);  //spd,legspd,tmrStep
  //void MajuKompasTimer(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int timerKompas) {
  lcd.setCursor(14, 1);
  lcd.print("MT");
  MajuKompasTimer(500, 500, 5, 50, 20, 250, 60);    //arah tanpa tembok
  lcd.setCursor(14, 1);
  lcd.print("WK");
  //void wall_kiri_modif(int speed, int s_sp, int skp, int skd, int xtimer, int setjarak4, int uvtrondetect) {
  sendall(7, 500, 500, 10);  //spd,legspd,tmrStep
  wall_kiri_modif(500, 67, 5, 4, 1, 42, false);

  lcd.setCursor(14, 1);       //Putar kanan sebelum piramid
  lcd.print("PK");
  sendall(7, 1000, 1000, 5);  //spd,legspd,tmrStep
  putarKanan(50, 1000, 60);

  lcd.setCursor(14, 1);      //Kalibarsi sudut putar
  lcd.print("KK");
  do {
    sendall(2, 20, -20, 500);  // muter kanan
    delay(5);
    read_sensor();
  } while (jarak1 < 800 || jarak3 < 800 || jarak5 < 800);

  reset_bno();
  lcd.setCursor(14, 1);
  lcd.print("MK");
  //void MajuKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int batasDepan) {
  //MajuKompas(128, 1024, 30, 50, 125, 40); //lewat piramid
  MajuKompasPiramidPertama(200, 1000, 20, 30, 120, 40, 35); //lewat piramid
  sendall(0, 0, 0, 500);
  lcd.setCursor(14, 1);
  lcd.print("PA");

  padamkan_api(3000);

  sendall(7, 1000, 1000, 5);  //spd,legspd,tmrStep
  lcd.setCursor(14, 1);      //Kalibarsi sudut putar
  lcd.print("KK");
  do {
    sendall(2, 35, -35, 1000);  // muter kanan
    delay(5);
    read_sensor();
  } while (jarak3 < 800);

  reset_bno();
  lcd.setCursor(13, 1);
  lcd.print("MPR");

  MajuKompasPiramid(200, 1000, 20, 50, 120, 60); //lewat piramid

  //------ Kalibrasi sudut   -------------

  lcd.setCursor(14, 1);
  lcd.print("KS");
  do {
    sendall(2, -50, 50, 500);  // muter kiri
    delay(5);
    sudut = baca_bno();
  } while (sudut > 2);

  //------ Maju hingga lantai hitam, turun dari piramid   -------------
  lcd.setCursor(14, 1);
  lcd.print("KT");
  do {
    sendall(1, 20, 20, 500); delay(1);
  } while (baca_bno_tinggi() < -371.0);

  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(128, 1024, 5, 20, 125, 62);


  //------ Belok kiri arah home   -------------
  lcd.setCursor(13, 1);      //Kalibarsi sudut  arah home
  lcd.print("HKK");
  do {
    sendall(2, -20, 20, 500);  // muter kiri
    read_sensor();
    if (jarak1 > 100 && jarak3 > 800 && jarak5 < 90) break;
  } while (true);
  sendall(0, 0, 0, 500);

  //------ Maju arah home   -------------
  reset_bno();
  lcd.setCursor(13, 1);
  lcd.print("HMK");
  MajuKompasHome(500, 500, 5, 50, 20, 25, 69);

  //------ Belok kiri ke arah karpet home -------------
  lcd.setCursor(13, 1);
  lcd.print("KPK");
  putarKiri(50, 200, -75);


  lcd.setCursor(13, 1);      //Kalibarsi sudut  arah kotak
  lcd.print("KKK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri
    delay(5);
    read_sensor();
    if (jarak1 > 100 && jarak3 > 60 ) break;
  } while (true);
  reset_bno();

  //------- Pulang ke home
  lcd.setCursor(13, 1);
  lcd.print("KWK");
  wall_kanan(180, 10, 5, 10, 5, 110, false);



  lcd.setCursor(10, 1);      //Kalibarsi sudut  arah kotak
  lcd.print("FINISH");
  sendall(0, 0, 0, 500);
  while (1) {}
  return;

  //////////RAMPUNG.......................
  ///?????
  do {
    sendall(2, -50, 50, 500);  // muter kiri
    delay(5);
    sudut = baca_bno();
  } while (sudut > 0);

  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(128, 1024, 5, 20, 125, 62);



  lcd.setCursor(14, 1);      //Kalibarsi sudut  arah home
  lcd.print("KK");
  do {
    sendall(2, -20, 20, 500);  // muter kiri
    delay(5);
    read_sensor();
    if (jarak1 > 800 && jarak3 > 800 && jarak5 < 80) break;
  } while (true);

  reset_bno();
  lcd.setCursor(14, 1);
  lcd.print("MK");

  MajuKompas(500, 500, 5, 50, 20, 20);

  lcd.setCursor(14, 1);      //Kalibarsi sudut  arah kotak
  lcd.print("KK");
  do {
    sendall(2, -20, 20, 500);  // muter kiri
    delay(5);
    read_sensor();
    if (jarak1 > 800 && jarak3 > 60 ) break;
  } while (true);

  reset_bno();
  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(500, 500, 5, 50, 20, 80);

  sendall(1, 0, 0, 500);
  return;
  //=====


  //cariBonekaPixy(20,15,185,3); //sudut cari 20 ke kanan dan kekiri, langkah kaki 15, batas pixy Y 185, offset 3 pixel
  //angkatKorban(0,0,45,60); //void angkatKorban(int turun, int buka, int ncapit, int naik) {
  //delay(5000);
  //taruhKorban(60,0,0);
  //standby_gripper();
  ////capit
  sendall(0, 0, 0, 500);
  return;





  //========================================================================== belajargerakbaru ======================
  // lcd.setCursor(14, 1);
  // lcd.print("MP");
  // MajuPID(350, 10, 5, 3, 28);

  // lcd.setCursor(14, 1);
  // lcd.print("PK");
  // putarKiri(70, 200, -60);

  // //kalibrasi sudut lewat jarak, tikungan 1
  // lcd.setCursor(14, 1);
  // lcd.print("KK");
  // do {
  //   sendall(2, -10, 10, 500);  // muter kiri
  //   delay(10);
  //   read_sensor();
  // } while (jarak1 < 800 || jarak3 < 100);
  // sendall(0, 0, 0, 512);  //berhenti

  // reset_bno();
  // lcd.setCursor(14, 1);
  // lcd.print("MK");
  // MajuKompas(128, 1024, 30, 60, 90, 23);

  // lcd.setCursor(14, 1);
  // lcd.print("PK");
  // putarKiri(70, 200, -80);

  // //kalibrasi sudut lewat jarak, tikungan 2
  // lcd.setCursor(14, 1);
  // lcd.print("KK");
  // do {
  //   sendall(2, -10, 10, 500);  // muter kiri pelan
  //   delay(10);
  //   read_sensor();
  // } while (jarak1 < 100);

  sendall(0, 0, 0, 512);  //berhenti dan reset bno
  reset_bno();

  lcd.setCursor(14, 1);
  lcd.print("NK");
  MajuNaikKompas(96, 1024, 16, 72, 128, 64);  //Tanjakan

  lcd.setCursor(14, 1);
  lcd.print("TK");
  MajuTurunKompas(96, 1024, 20, 15, 85, 50);  //Turunan

  //kalibrasi hingga selesai turun lewat jarak depan
  lcd.setCursor(14, 1);
  lcd.print("KT");
  MajuKompas(128, 1024, 30, 60, 90, 23);

  // do {
  //   sendall(6, 50, 50, 85);
  //   delay(10);
  //   read_sensor();
  // } while (jarak3 > 65 || jarak5 < 800);

  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(50, 200, -80);

  //kalibrasi sudut lewat jarak, tikungan 2
  lcd.setCursor(14, 1);
  lcd.print("KK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri pelan
    delay(10);
    read_sensor();
  } while (jarak1 < 100 || jarak3 < 100);

  sendall(0, 0, 0, 512);  //berhenti dan reset bno
  reset_bno();

  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(256, 512, 5, 50, 35, 65);

  lcd.setCursor(14, 1);
  lcd.print("PK");
  putarKiri(70, 200, -80);

  //kalibrasi sudut lewat jarak depan dan kiri, tikungan 4
  lcd.setCursor(14, 1);
  lcd.print("KK");
  do {
    sendall(2, -10, 10, 500);  // muter kiri
    delay(10);
    read_sensor();
  } while (jarak1 < 800 && jarak3 < 100);

  //maju rintangan random 2
  lcd.setCursor(14, 1);
  lcd.print("MK");
  MajuKompas(240, 1024, 30, 60, 90, 30);

  //wall_kanan(int speed, int s_sp, int skp, int skd, int xtimer, int setjarak, int uvtrondetect)
  //cari api
  majuCariApi(20, 100, 80);


  while (apiTerdeteksi) {
    Serial.print("API");
    flame_biner = read_flame();
    if ((flame_biner & 0b0000000001111111) > 0) {
      // Serial.print(flame_biner,BIN);
      Serial.println(">");
      sendall(2, 50, -50, 200);
    } else if ((flame_biner & 0b1111111000000000) > 0) {
      // Serial.print(flame_biner,BIN);
      Serial.println("<");
      sendall(2, -50, 50, 200);
    } else if ((flame_biner & 0b0000000110000000) > 0) {
      Serial.println("=");
      sendall(1, 10, 10, 100);
      read_uvtron();
      delay(200);
      if (counter > 4) break;
    }
  }


  sendall(0, 0, 0, 500);  //berhenti


  sendall(0, 0, 0, 512);  //berhenti
  return;


  MajuCounterKompas(64, 1024, 16, 15, 85, 1000);  //Setelahturun
  MajuKompas(120, 1024, 30, 60, 90, 55);
  putarKiri(50, 200, -80);
  MajuPID(300, 10, 5, 3, 40);
  putarKiri(50, 200, -80);
  reset_bno();
  MajuKompas(120, 1024, 30, 60, 90, 45);

  //wall_kanan(200, 17, 8, 3, 18, 1000, false);
  //putarKiri(50, 200, -90);
  //MajuNaik( tinggiKaki, setPoint, KP, KD,  jarakHenti, bataskirikanan)
  //MajuNaik(100, 20, 8, 3, 18, 60);

  //MajuNaikKompas(servoMoveSpeed,servoUpSpeed,servoStepDelay,servoSudutStep,tinggiKaki,bataskirikanan){
  //MajuTurunKompas(servoMoveSpeed,servoUpSpeed,servoStepDelay,servoSudutStep,tinggiKaki,batasDepan){
  //MajuCounterKompas(servoMoveSpeed,servoUpSpeed,servoStepDelay,servoSudutStep,tinggiKaki,counter){
  reset_bno();                                    //
  MajuNaikKompas(100, 1024, 20, 70, 100, 60);     //Tanjakan
  MajuTurunKompas(64, 1024, 16, 15, 85, 30);      //Turunan
  MajuCounterKompas(64, 1024, 16, 15, 85, 1000);  //Setelahturun

  sendall(0, 0, 0, 512);  //berhenti
  return;

  //majuEasy(20, 20, 120, 45);

  //  majuEasy(50, 240, 120, 5);

  sendall(0, 0, 0, 500);  //berhenti
  return;

  Serial.println("Start");
  majuCariApi(20, 100, 80);


  while (apiTerdeteksi) {
    Serial.print("API");
    flame_biner = read_flame();
    if ((flame_biner & 0b0000000001111111) > 0) {
      // Serial.print(flame_biner,BIN);
      Serial.println(">");
      sendall(2, 50, -50, 200);
    } else if ((flame_biner & 0b1111111000000000) > 0) {
      // Serial.print(flame_biner,BIN);
      Serial.println("<");
      sendall(2, -50, 50, 200);
    } else if ((flame_biner & 0b0000000110000000) > 0) {
      Serial.println("=");
      sendall(1, 10, 10, 100);
      read_uvtron();
      delay(200);
      if (counter > 4) break;
    }
  }

  sendall(0, 0, 0, 500);  //berhenti
  return;


  //MajuPID(spped, jarakWallKiri/Kanan, KProp, KDef, jarakHenti)
  MajuPID(200, 20, 8, 3, 25);

  //majuEasy(kecepatan_maju, ketinggian_kaki, jarakHenti)
  //majuEasy(50, 100, 15);

  putarKiri(50, 200, -75);
  MajuPID(200, 20, 8, 3, 25);
  putarKanan(50, 200, 75);
  MajuPID(200, 20, 8, 3, 25);
  putarKiri(50, 200, -75);
  // majuEasy(50, 100, 15);

  sendall(0, 0, 0, 500);  //berhenti
  return;


  sendall(0, 0, 0, 500);  //berhenti
  return;

  //--------------------------------------
  mode_start1();
  //reset_bno();
  kanan_delay(15);
  geser_kiri(60);
  wall_kanan(200, 17, 8, 3, 18, 1000, false);
  reset_bno();
  wall_kiri(500, 15, 20, 40, 41, 20, false);
  kompas_kanan(100);
  wall_kiri(350, 16, 14, 30, 15, 10000, false);
  wall_kiri(550, 11, 40, 70, 50, 1000, false);
  wall_kiri(200, 11, 40, 70, 10, 16, false);
  kompas_kanan(170);
  kanan_delay(20);
  wall_kiri(250, 15, 40, 25, 15, 19, false);
  kompas_kanan(-70);
  wall_kiri(300, 13, 15, 30, 25, 10000, false);
  wall_kiri(260, 24, 15, 25, 3, 10000, true);

  ////  MASUK GERBANG RUANG 1
  kompas_kanan(-100);
  maju_delay(40);
  kompas_kanan(-90);
  maju_delay(50);
  kompas_kanan(-85);
  maju_delay(80);
  kompas_kanan(-85);
  maju_delay(80);

  //////////MENGAKURATKAN KE ARAH SAFTY ZONE/////
  kompas_kiri(-150);
  kiri_delay(20);
  wall_kanan(400, 13, 5, 10, 8, 16, false);
  mundur_delay(10);
  kiri_delay(110);
  kompas_kiri(5);
  wall_kiri(250, 16, 5, 10, 20, 80, true);
  mundur_delay(20);
  padamkan_api(4000);

  /////////// DIAKTIFKAN KETIKA AKAN MENGAMBIL KORBAN MENGGUNAKAN GRIPER
  maju_delaya(30);
  kanan_delay(30);
  geser_kanan(20);
  //      kanan_delay(25);
  //      geser_kanan(20);
  //       mundur_delaya(30);

  /////////// MENCARI KORBAN  /////////////
  int ya = posisi_boneka_kiri(75);
  if (ya == 1) {  // boneka detek
    geser_kanan(50);
    kiri_delay(18);
    //ambil_bonekaa();
  }

  else {  // boneka tidak detek
    kiri_delay(50);

    int ya1 = posisi_boneka_kanan(250);
    if (ya1 == 1) {  // boneka detek
      mundur_delaya(20);
      //ambil_bonekan();

    }

    else {  // boneka tidak detek
    }
  }

  mundur_delay(90);
  kiri_delay(90);
  kompas_kiri(1);
  //// MENGHADAP SAFTY ZONE ///
  wall_kanan(250, 15, 5, 10, 12, 34, false);
  //maju_delayb(20);
  turun_gripper();
  delay(1000);
  //lepas_grippera();
  delay(10);
  mundur_delaya(80);
  kanan_delay(10);
  naik_gripper();
  delay(10);
  standby_gripper();
  mundur_delaya(80);

  /////////  SETELAH TAROH KORBAN
  mundur_delay(10);
  kiri_delay(110);
  kompas_kiri(5);
  wall_kanan(250, 14, 5, 10, 50, 1000, false);

  //// KELUAR RUANG 1
  kanan_delay(20);
  maju_delay(40);
  kanan_delay(20);

  /////// MASUK KARPET PERTIGAAN////////
  wall_kiri(500, 12, 14, 10, 30, 10000, false);
  wall_kiri(500, 15, 14, 10, 10, 10000, false);
  wall_kanan(500, 11, 20, 30, 10, 10000, false);
  wall_kanan(550, 11, 40, 70, 10, 14, false);

  //// masuk ruang 2 ///
  kiri_delay(30);
  kompas_kiri(-10);
  wall_kanan(200, 12, 8, 15, 24, 90, true);
  mundur_delay(45);
  padamkan_api(4000);
  maju_delay(45);

  /////   BALIK HOME/////////////
  kompas_kiri(-160);
  wall_kiri(400, 14, 6, 10, 12, 20, false);
  kompas_kanan(-90);
  wall_kiri(500, 13, 3, 5, 20, 1000, false);
  wall_kiri(500, 15, 40, 70, 48, 16, false);
  kompas_kanan(5);
  wall_kiri(450, 13, 5, 10, 16, 1000, false);
  stop_permanent();
}
















///  SETINGAN GRIPER
//        int ya = posisi_boneka_kanan(80);
//
//        if( ya == 1 ){ // boneka detek
//         ambil_boneka();
//        }
//        else{ // boneka tidak detek
//         kompas_kanan(100);
//        int ya1 = posisi_boneka_kiri(80);
//        if( ya1 == 1 ){ // boneka detek
//         ambil_boneka();
//         }
//         else{ // boneka tidak detek

//          }

//          }

//      mundur_delay(50);
//      kompas_kiri(20);
//      geser_kanan(170);
//      kompas_kiri(20);
//     geser_kanan(100);
//



//      wall_kiri(400, 13, 5, 10, 30, 60, false);

/////// MASUK KARPET PERTIGAAN////////
//      wall_kiri(500, 12,14, 10, 22 ,10000, false);
//      wall_kanan(500, 14, 40, 70, 10, 14, false);

//// masuk ruang 2 ///
//     kiri_delay(20);
//     kompas_kiri(1);
//      wall_kanan(300, 12, 8 , 15, 26, 90, true);
//      padamkan_api(3500);






/////   BALIK HOME/////////////
//      kompas_kiri(-160);
//      wall_kiri(400,14, 6, 10, 12, 20, false);


//      kompas_kanan(-90);
//     wall_kiri(500, 13, 3, 5, 20, 1000, false);
//      wall_kiri(500, 15, 40, 70, 48, 16, false);
//       kompas_kanan(5);
//      wall_kiri(450, 13, 5, 10, 16, 1000, false);


void RCheck(int speed, int s_sp, int skp, int skd, int jarakHenti) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {
    int j4 = jarak4 - 13;
    pv = min(j4, jarak5);
    error = sp - pv;
    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("R");
    lcd.print(sp);
    lcd.print(" ");
    lcd.print(pv);
    lcd.print(" ");
    lcd.print(jarak3);

    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);
    sendall(1, LOUT, ROUT, speed);  // jalan

    read_sensor();
    if (jarak3 < jarakHenti) {
      break;
    }
  }
}
#define LEBARROBOT 30
void MajuPID(int speed, int s_sp, int skp, int skd, int jarakHenti) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {

    //R
    int j4 = jarak4 - 13;
    pv = min(j4, jarak5);
    error = sp - pv;
    Serial.print("R");
    Serial.println(pv);

    //R-Far --> L
    if (jarak4 > (LEBARROBOT + sp)) {
      int j2 = jarak2 - 13;
      pv = min(jarak1, j2);
      error = pv - sp;
      Serial.print("L");
      Serial.println(pv);
    }

    //L/R-Far -->straight
    if (jarak2 > (LEBARROBOT + sp)) {
      pv = sp;
      error = 0;
      Serial.print("LR");
      Serial.println(pv);
    }

    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("LR");
    lcd.print(sp);
    lcd.print(" ");
    lcd.print(pv);
    lcd.print(" ");
    lcd.print(jarak3);

    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);
    sendall(1, LOUT, ROUT, speed);  // jalan

    read_sensor();
    if (jarak3 < jarakHenti || jarak2 < 10 || jarak4 < 10) {
      break;
    }
  }
}

void LCheck(int speed, int s_sp, int skp, int skd, int jarakHenti) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {
    int j2 = jarak2 - 13;
    pv = min(jarak1, j2);
    error = pv - sp;
    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("L");
    lcd.print(sp);
    lcd.print(" ");
    lcd.print(pv);
    lcd.print(" ");
    lcd.print(jarak3);

    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);

    sendall(1, LOUT, ROUT, speed);  // jalan
    read_sensor();
    if (jarak3 < jarakHenti) {
      break;
    }
  }
}

void servoStandby(int naik, int ncapit) {  //servonaik semakin besar semakin tinggi, servocapit semakin besar semakin mencapit
  updown.write(naik);
  capit.write(ncapit);
}

void angkatKorban(int turun, int buka, int ncapit, int naik) {
  //turun
  updown.write(turun);
  capit.write(buka);
  delay(500);
  //capit
  updown.write(turun);
  capit.write(ncapit);
  delay(200);
  capit.write(ncapit);
  delay(200);
  //angkat
  updown.write(naik);
}

void taruhKorban(int naik, int turun, int buka) {
  //taruh
  for (int x = 0; x < (naik - turun); x += 3) {
    updown.write(turun - x);
    delay(100);
  }
  capit.write(buka);
  delay(500);
}

void MajuCounterKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int batascounter) {
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  int counters = 0;
  do {
    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(counters);
    counters++;
  } while (counters < batascounter);
}
void MajuNaikKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int bataskirikanan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  delay(50);
  sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    //arif
    // float sudut = baca_bno();

    // if (jarak1 <= 12 || jarak2 <= 16) {
    //   LOUT = 65;
    //   ROUT = 0;
    // } else if (jarak5 <= 12 || jarak4 <= 16) {
    //   LOUT = 0;
    //   ROUT = 65;
    // } else if (sudut > 10) {
    //   LOUT = 5;
    //   ROUT = 60;
    // } else if (sudut < -5) {
    //   LOUT = 60;
    //   ROUT = 5;
    // } else {
    //   LOUT = sudutStep;
    //   ROUT = sudutStep;
    // }

float sudut = baca_bno();
    if (jarak1 <= 12 || jarak2 <= 16) {
      LOUT = 65;
      ROUT = 0;
    } else if (jarak5 <= 12 || jarak4 <= 16) {
      LOUT = 0;
      ROUT = 65;
    } else if (sudut > 10) {
      LOUT = 1;
      ROUT = 60;
    } else if (sudut < -9) {
      LOUT = 60;
      ROUT = 1;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }
    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    char buffer[40];
    sprintf(buffer, "e%d L%d R%d s%d jl%d jr%d ", error, LOUT, ROUT, sudutPID, jarak1, jarak5);
    Serial.println(buffer);
    float ketinggian = baca_bno_tinggi();
    if (jarak1 < 10 && ketinggian > 5) {
      LOUT = 70; ROUT = 0;
      //sendall(4, LOUT, ROUT, moveSpeed);  // geser
      //delay(100);
    } else if (jarak5 < 10 && ketinggian > 5) {
      //sendall(3, LOUT, ROUT, moveSpeed);  // jalan
      LOUT = 0; ROUT = 70;
      //delay(100);
    }
    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(error);
    lcd.print(F(" "));
    lcd.print(LOUT);
    lcd.print(F(" "));
    lcd.print(ROUT);
    lcd.print(F(" "));
    lcd.setCursor(0, 1);
    lcd.print(sudutPID);
    lcd.print(" ");
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(jarak5);
    sudutPID = baca_bno();
    if (baca_bno_tinggi() < -373.0) break;
  } while (true);
}
void MajuTurunKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int bataskirikanan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  delay(200);
  sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (sudut > 10) {
      LOUT = -15;
      ROUT = 75;
    } else if (sudut < -5) {
      LOUT = 75;
      ROUT = -15;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }
    if (jarak1 <= 12) {
      LOUT = 0; ROUT = -15;
    } else if (jarak5 <= 12 ) {
      LOUT = -15; ROUT = 0;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    char buffer[40];
    sprintf(buffer, "e%d L%d R%d s%d jd%d ", error, LOUT, ROUT, sudutPID, jarak3);
    Serial.println(buffer);
    sudutPID = baca_bno_tinggi();
    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(sudut);
    lcd.print(F(" "));
    lcd.print(LOUT);
    lcd.print(F(" "));
    lcd.print(ROUT);
    lcd.print(F(" "));
    lcd.setCursor(0, 1);
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(sudutPID);
    lcd.print(" ");
    lcd.print(jarak5);
    lcd.print(" TK");
    // if(jarak3 <= 12 && sudut < -45) //robot hadap kiri (no wall) saat turun
    // {
    //   sendall(6, 0, -50, tinggiKaki);  // jalan
    //   delay(10);
    //   do{read_sensor();}while(baca_bno()<0);
    // }
    // if(jarak3 <= 12 && sudut > 45) //robot hadap kanan (no wall) saat turun
    // {
    //   sendall(6, -50, 0, tinggiKaki);  // jalan
    //   delay(10);
    //   do{read_sensor();}while(baca_bno()>0);
    // }
    if(baca_bno_tinggi()>5 && (sudut > 80 || sudut < -90)){  //robot hadap tanjakan
      sendall(6,-sudutStep,-sudutStep,tinggiKaki);
      while(baca_bno_tinggi()>0);
      while(baca_bno() < 0) {
        sendall(2, 50, -50, tinggiKaki);  // putar hadap karpet
      }
    }
    if(sudutPID > -370 && jarak1 > bataskirikanan && jarak5 > bataskirikanan && jarak3 > 35) break; //hadap kiri /kanan
    //if (jarak1 > bataskirikanan && jarak5 > bataskirikanan) break;
  } while (true);
}

void MajuKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int batasDepan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  // delay(200);
  // sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (jarak1 <= 15 || jarak2 <= 30) {
      LOUT = 50;
      ROUT = 0;
    } else if (jarak5 <= 15 || jarak4 <= 30) {
      LOUT = 0;
      ROUT = 50;
    } else if (sudut > 10) {
      LOUT = 0;
      ROUT = 55;
    } else if (sudut < -9) {
      LOUT = 55;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    //sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    sendall(8, LOUT, ROUT, moveSpeed);  // jalan
    char buffer[26];
    sprintf(buffer, "e%d L%d R%d s%d jd%d", jarak1, LOUT, ROUT, sudutPID, jarak3);
    Serial.println(buffer);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    if (jarak3 < batasDepan) break;
  } while (true);
}
void MajuKompasHome(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int batasDepan, int bataskananmin) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  // delay(200);
  // sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (jarak1 <= 15 || jarak2 <= 30) {
      LOUT = 50;
      ROUT = 0;
    } else if (jarak5 <= 15 || jarak4 <= 30) {
      LOUT = 0;
      ROUT = 50;
    } else if (sudut > 10) {
      LOUT = 0;
      ROUT = 55;
    } else if (sudut < -9) {
      LOUT = 55;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    //sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    sendall(8, LOUT, ROUT, moveSpeed);  // jalan
    char buffer[26];
    sprintf(buffer, "e%d L%d R%d s%d jd%d", jarak1, LOUT, ROUT, sudutPID, jarak3);
    Serial.println(buffer);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    if (jarak3 < batasDepan) break;
    if (jarak5 > bataskananmin) {
      sendall(4, 0, 0, moveSpeed);
      delay(stepDelay);
    }
  } while (true);
}

void MajuKompas(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int batasDepan, int makDepan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  // delay(200);
  // sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (jarak1 <= 15 || jarak2 <= 30) {
      LOUT = 50;
      ROUT = 0;
    } else if (jarak5 <= 15 || jarak4 <= 30) {
      LOUT = 0;
      ROUT = 50;
    } else if (sudut > 10) {
      LOUT = 0;
      ROUT = 55;
    } else if (sudut < -9) {
      LOUT = 55;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    //sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    sendall(8, LOUT, ROUT, moveSpeed);  // jalan
    char buffer[26];
    sprintf(buffer, "e%d L%d R%d s%d jd%d", jarak1, LOUT, ROUT, sudutPID, jarak3);
    Serial.println(buffer);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    if (jarak3 < batasDepan || jarak3 > makDepan) break;
  } while (true);
}

void MajuKompasPiramid(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int kiriKanan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep

  // sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  // delay(200);
  // sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (jarak1 <= 15 || jarak2 <= 30) {
      LOUT = 70;
      ROUT = 0;
    } else if (jarak5 <= 15 || jarak4 <= 30) {
      LOUT = 0;
      ROUT = 70;
    } else if (sudut > 10) {
      LOUT = 0;
      ROUT = 75;
    } else if (sudut < -9) {
      LOUT = 75;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    //sendall(8, LOUT, ROUT, 200);  // jalan
    char buffer[26];
    sprintf(buffer, "ji%d L%d R%d s%d ja%d", jarak1, LOUT, ROUT, sudutPID, jarak5);
    Serial.println(buffer);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    if ((jarak1 > kiriKanan && jarak2 > kiriKanan) || ( jarak5 > kiriKanan && jarak4 > kiriKanan)) break;
  } while (true);
}

void MajuKompasPiramidPertama(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int kiriKanan, int depan) {
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);  //serVal,spd,legspd,tmrStep
  // delay(200);
  // sendall(6, sudutStep, sudutStep, tinggiKaki);  // jalan
  do {
    float sudut = baca_bno();
    if (jarak1 <= 15 || jarak2 <= 30) {
      LOUT = 70;
      ROUT = 0;
    } else if (jarak5 <= 15 || jarak4 <= 30) {
      LOUT = 0;
      ROUT = 70;
    } else if (sudut > 10) {
      LOUT = 0;
      ROUT = 75;
    } else if (sudut < -9) {
      LOUT = 75;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }

    // error=sudutPID - 0;
    // out_pid();
    // LOUT = sudutStep - out_p - out_d;
    // ROUT = sudutStep + out_p + out_d;
    // if (LOUT > 50) LOUT = 50;
    // if (LOUT < -15) LOUT = -15;
    // if (ROUT > 50) ROUT = 50;
    // if (ROUT < -15) ROUT = -15;


    read_sensor();
    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    //sendall(8, LOUT, ROUT, 200);  // jalan
    char buffer[26];
    sprintf(buffer, "2:%d 3:%d 4:%d", jarak2, jarak3, jarak4);
    Serial.println(buffer);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    if (jarak3 < 35 && jarak3 > 25) {
      do {
        sudut = baca_bno();
        if (sudut < -10) {
          sendall(2, sudutStep, -1 * sudutStep, 200); // putar kanan
          delay(100);
        }
        if (sudut > 10) {
          sendall(2, -1 * sudutStep, sudutStep, 200); // putar kiri
          delay(100);
        }
        sudut = baca_bno();
      } while (sudut < -10 && sudut > 10);
    }
    read_sensor();
    if ( jarak3 > 25 && jarak3 < 35 ) break;
    //if ((jarak1 < kiriKanan && jarak3 < depan && jarak5 < kiriKanan) && sudut  > -15 && sudut < 15) break;
  } while (true);
}


void MajuNaik(int tinggiKaki, int s_sp, int skp, int skd, int jarakHenti, int bataskirikanan) {
  sendall(7, 256, 1024, 16); // serVal,spd,legspd,tmrStep
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  while (1) {
    int j2 = jarak2 - 13;
    pv = min(jarak1, j2);
    error = pv - sp;
    out_pid();
    LOUT = 25 - out_p - out_d;
    ROUT = 25 + out_p + out_d;
    if (LOUT > 75) LOUT = 75;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 75) ROUT = 75;
    if (ROUT < -15) ROUT = -15;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("L");
    lcd.print(sp);
    lcd.print(" ");
    lcd.print(pv);
    lcd.print(" ");
    lcd.print(jarak3);

    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(jarak1);
    lcd.print(" ");
    lcd.print(jarak5);

    sendall(6, LOUT, ROUT, tinggiKaki);  // jalan
    read_sensor();

    if (jarak3 < jarakHenti || (jarak1 > bataskirikanan && jarak5 > bataskirikanan)) {
      sendall(7, 0, 0, 5);
      break;
    }
  }
}

void wall_kiri_modif(int speed, int s_sp, int skp, int skd, int xtimer, int setjarak, int uvtrondetect) {
  int cnttimer = 0;
  sp = s_sp;
  kp = skp;
  kd = skd;
  int lastJarak1 = jarak1;
  sendall(1, LOUT, ROUT, speed);  // jalan
  while (1) {
    //int j2 = jarak2 - 13;
    if (jarak1 < (s_sp - 20) || jarak1 > 800)
      jarak1 = (s_sp - 20);
    else
      lastJarak1 = jarak1;
    pv = jarak1; //pv = min(jarak1, j2);
    error = pv - sp;
    out_pid();
    LOUT = 50 - out_p - out_d;
    ROUT = 50 + out_p + out_d;
    if (LOUT > 50) LOUT = 50;
    if (LOUT < -15) LOUT = -15;
    if (ROUT > 50) ROUT = 50;
    if (ROUT < -15) ROUT = -15;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Timer: ");
    lcd.print(cnttimer);
    lcd.setCursor(0, 1);
    lcd.print(error);
    lcd.print(" ");
    lcd.print(LOUT);
    lcd.print(" ");
    lcd.print(ROUT);
    sendall(1, LOUT, ROUT, speed);  // jalan
    read_sensor();
    if (cnttimer < xtimer) {
      cnttimer++;
      lcd.backlight();
    } else {
      //lcd.noBacklight();
    }

    if (uvtrondetect == 1) {
      if (jarak4 < setjarak && cnttimer >= xtimer && counter > 0) break;
      read_uvtron();
    } else {
      if (setjarak > 0) {
        if (jarak4 < setjarak && cnttimer >= xtimer) break;
        if (jarak4 > 100 && jarak2 > 125) break;
      } else {
        if (cnttimer >= xtimer) break;
      }
    }
  }
}

void MajuKompasTimer(int moveSpeed, int upSpeed, int stepDelay, int sudutStep, int tinggiKaki, int timerKompas, int bataskiri) {
  reset_bno();
  kp = 7;
  kd = 3;
  float sudutPID = 0;
  sendall(7, moveSpeed, upSpeed, stepDelay);
  int timerCounter = 0;
  do {
    float sudut = baca_bno();
    if (sudut > 10) {
      LOUT = 0;
      ROUT = sudutStep;
    } else if (sudut < -9) {
      LOUT = sudutStep;
      ROUT = 0;
    } else {
      LOUT = sudutStep;
      ROUT = sudutStep;
    }
    if (jarak1 < bataskiri && bataskiri > 0 && jarak1 > 35) sendall(4, 0, 0, moveSpeed); else
      sendall(1, LOUT, ROUT, moveSpeed);
    read_sensor();
    char buffer[16];
    sprintf(buffer, "L%d R%d s%d t%d", LOUT, ROUT, sudutPID, timerCounter);
    Serial.println(buffer);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(buffer);
    sudutPID = baca_bno();
    timerCounter = timerCounter + 10;
    if (timerCounter >= timerKompas) break;
    delay(10);
  } while (true);
}
